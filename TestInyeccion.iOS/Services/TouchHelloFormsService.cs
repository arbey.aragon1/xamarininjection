﻿using System;
using TestInyeccion.Interfaces;

namespace TestInyeccion.iOS.Services
{
    public class TouchHelloFormsService : IHelloFormsService
    {
        public string GetHelloFormsText()
        {
            return "Hello from iOS";
        }
    }
}
