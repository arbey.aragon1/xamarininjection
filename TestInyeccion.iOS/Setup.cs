﻿using System;
using Autofac;
using TestInyeccion.Interfaces;
using TestInyeccion.iOS.Services;
using TestInyeccion.Setup;

namespace TestInyeccion.iOS
{
    public class Setup : AppSetup
    {
        protected override void RegisterDependencies(ContainerBuilder container)
        {
            base.RegisterDependencies(container);
            container.RegisterType<TouchHelloFormsService>().As<IHelloFormsService>();
        }
    }
}
