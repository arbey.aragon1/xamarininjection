﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace TestInyeccion.Interfaces
{
    public interface INavigation
    {
        Task<Page> PopAsync();
        Task<Page> PopModalAsync();
        Task PopToRootAsync();
        Task PushAsync(Page page);
        Task PushModalAsync(Page page);
    }
}
