﻿using System;
namespace TestInyeccion.Interfaces
{
    public interface IHelloFormsService
    {
        string GetHelloFormsText();
    }
}
