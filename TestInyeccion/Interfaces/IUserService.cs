﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestInyeccion.Models;

namespace TestInyeccion.Interfaces
{
    public interface IUserService
    {
        Task AddUser(
            string email,
            string lastName,
            string dateOfBirth);

        Task SetUser(
            string uid,
            string name,
            string email,
            string lastName,
            string dateOfBirth);

        Task<List<User>> FetchUserList();
        Task<User> FetchUserByUID(string uid);
    }
}
