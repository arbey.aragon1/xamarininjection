﻿using System;
using System.Threading.Tasks;
using Firebase.Auth;
using Firebase.Database;

namespace TestInyeccion.Interfaces
{
    public interface IFirebaseService
    {
        Task<FirebaseAuthLink> CreateUserWithEmailAndPasswordAsync(string email, string password);
        Task<FirebaseAuthLink> SignInUserWithEmailAndPasswordAsync(string email, string password);
        FirebaseAuthLink GetAuthData();
        string GetUserUID();
        FirebaseClient GetDBReference();
        bool HasAuth { get; set; }
    }
}
