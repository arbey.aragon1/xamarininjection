﻿using System;
using Autofac;
using TestInyeccion.Interfaces;
using TestInyeccion.Services;
using TestInyeccion.Setup.Modules;
using TestInyeccion.ViewModels;
using TestInyeccion.Views;
using Xamarin.Forms;

namespace TestInyeccion.Setup
{
    public class AppSetup
    {

        public IContainer CreateContainer(App app)
        {

            var builder = new ContainerBuilder();


            RegisterDependencies(builder);


            var container = builder.Build();

            var viewFactory = container.Resolve<IViewFactory>();

            viewFactory.Register<LandingViewModel, LandingPage>();
            viewFactory.Register<HomeBaseViewModel, HomePage>();
            viewFactory.Register<SecondViewModel, SecondPage>();
            viewFactory.Register<RegistrationViewModel, RegistrationPage>();
            viewFactory.Register<LoginViewModel, LoginPage>();
            viewFactory.Register<ProfileViewModel, ProfilePage>();
            viewFactory.Register<ProfileEditorViewModel, ProfileEditorPage>();
            viewFactory.Register<DocumentViewModel, DocumentPage>();
            viewFactory.Register<MedicalRecordViewModel, MedicalRecordPage>();
            viewFactory.Register<MedicalRecordEditorViewModel, MedicalRecordEditorPage>();
            viewFactory.Register<MainMenuViewModel, MainMenuPage>();

            var mainPage = viewFactory.Resolve<LandingViewModel>();
            var navPage = new NavigationPage(mainPage);

            app.MainPage = navPage;

            return container;

        }

        protected virtual void RegisterDependencies(ContainerBuilder builder)
        {
            builder.RegisterModule<DependencyRegistrationModule>();
            builder.RegisterModule<ViewModelViewRegistrationModule>();
        }

    }
}
