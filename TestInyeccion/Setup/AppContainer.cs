﻿using System;
using Autofac;

namespace TestInyeccion.Setup
{
    public static class AppContainer
    {
        public static IContainer Container { get; set; }
    }
}
