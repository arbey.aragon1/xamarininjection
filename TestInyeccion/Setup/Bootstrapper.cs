﻿using System;
using Autofac;
using TestInyeccion.Interfaces;
using TestInyeccion.Setup.Modules;
using TestInyeccion.ViewModels;
using TestInyeccion.Views;
using Xamarin.Forms;

namespace TestInyeccion.Setup
{
    public class Bootstrapper
    {
        private readonly App _app;

        public Bootstrapper(App app)
        {
            _app = app;
        }

        public void Run()
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule<DependencyRegistrationModule>();
            builder.RegisterModule<ViewModelViewRegistrationModule>();

            var container = builder.Build();

            var viewFactory = container.Resolve<IViewFactory>();


            viewFactory.Register<HomeBaseViewModel, HomePage>();
            viewFactory.Register<SecondViewModel, SecondPage>();

            var mainPage = viewFactory.Resolve<HomeBaseViewModel>();
            var navPage = new NavigationPage(mainPage);

            _app.MainPage = navPage;
        }


    }
}
