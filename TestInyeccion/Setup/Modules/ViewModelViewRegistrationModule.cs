﻿using System;
using Autofac;
using TestInyeccion.Services;
using TestInyeccion.ViewModels;
using TestInyeccion.Views;

namespace TestInyeccion.Setup.Modules
{
    public class ViewModelViewRegistrationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {

            builder.RegisterType<CoreHelloFormService>().SingleInstance();

            builder.RegisterType<LandingPage>().SingleInstance();
            builder.RegisterType<LandingViewModel>().SingleInstance();

            builder.RegisterType<HomePage>().SingleInstance();
            builder.RegisterType<HomeBaseViewModel>().SingleInstance();

            builder.RegisterType<SecondPage>().SingleInstance();
            builder.RegisterType<SecondViewModel>().SingleInstance();


            builder.RegisterType<RegistrationPage>().SingleInstance();
            builder.RegisterType<RegistrationViewModel>().SingleInstance();

            builder.RegisterType<LoginPage>().SingleInstance();
            builder.RegisterType<LoginViewModel>().SingleInstance();

            builder.RegisterType<ProfilePage>().SingleInstance();
            builder.RegisterType<ProfileViewModel>().SingleInstance();

            builder.RegisterType<ProfileEditorPage>().SingleInstance();
            builder.RegisterType<ProfileEditorViewModel>().SingleInstance();

            builder.RegisterType<DocumentPage>().SingleInstance();
            builder.RegisterType<DocumentViewModel>().SingleInstance();

            builder.RegisterType<MedicalRecordPage>().SingleInstance();
            builder.RegisterType<MedicalRecordViewModel>().SingleInstance();

            builder.RegisterType<MedicalRecordEditorPage>().SingleInstance();
            builder.RegisterType<MedicalRecordEditorViewModel>().SingleInstance();

            builder.RegisterType<MainMenuPage>().SingleInstance();
            builder.RegisterType<MainMenuViewModel>().SingleInstance();
        }
    }
}
