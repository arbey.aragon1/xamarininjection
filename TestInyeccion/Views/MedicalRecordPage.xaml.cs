﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestInyeccion.Interfaces;
using TestInyeccion.ViewModels;
using Xamarin.Forms;

namespace TestInyeccion.Views
{
    public partial class MedicalRecordPage : ContentPage
    {
        private readonly INavigator _navigator;
        private readonly IUserService _userService;
        private readonly IAuthService _authService;

        public MedicalRecordPage(
            INavigator navigator,
            IUserService userService,
            IAuthService authService)
        {
            _userService = userService;
            _authService = authService;
            _navigator = navigator;
            InitializeComponent();
            medicalRecordEditorBtn.Clicked += async (sender, e) => {
                await GoMedicalRecordEditorButton(sender, e);
            };

        }


        public async Task<int> GoMedicalRecordEditorButton(object sender, EventArgs e)
        {
            await _navigator.PushAsync<MedicalRecordEditorViewModel>();
            return 0;
        }
    }
}
