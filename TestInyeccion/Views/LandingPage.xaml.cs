﻿using System;
using System.Collections.Generic;
using TestInyeccion.Interfaces;
using TestInyeccion.ViewModels;
using Xamarin.Forms;

namespace TestInyeccion.Views
{
    public partial class LandingPage : ContentPage
    {
        private readonly INavigator _navigator;

        public LandingPage(INavigator navigator)
        {
            Console.WriteLine("LandingPage");
            _navigator = navigator;
            InitializeComponent();
        }

        void OnRegistrationButton(object sender, EventArgs e)
        {
            Console.WriteLine("OnRegistrationButton");
            _navigator.PushAsync<RegistrationViewModel>();
        }

        void OnLoginButton(object sender, EventArgs e)
        {
            Console.WriteLine("OnLoginButton");
            _navigator.PushAsync<LoginViewModel>();
        }/**/
    }
}
