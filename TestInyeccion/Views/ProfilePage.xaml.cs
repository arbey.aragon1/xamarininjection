﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Firebase.Auth;
using TestInyeccion.Interfaces;
using TestInyeccion.ViewModels;
using Xamarin.Forms;

namespace TestInyeccion.Views
{
    public partial class ProfilePage : ContentPage
    {
        private readonly INavigator _navigator;
        private readonly IUserService _userService;
        private readonly IAuthService _authService;

        public ProfilePage(
            INavigator navigator,
            IUserService userService,
            IAuthService authService)
        {
            _userService = userService;
            _authService = authService;
            _navigator = navigator;
            InitializeComponent();

        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            string uid = _authService.GetUserUID();

            var user = await _userService.FetchUserByUID(uid);

            userName.Text = user.Name +" "+ user.LastName;
            dateOfBirth.Text = user.DateOfBirth;
            email.Text = user.Email;

            profileEditorBtn.Clicked += async (sender, e) => {
                await GoProfileEditorButton(sender, e);
            };

        }


        public async Task<int> GoProfileEditorButton(object sender, EventArgs e)
        {
            await _navigator.PushAsync<ProfileEditorViewModel>();
            return 0;
        }
    }
}
