﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Firebase.Auth;
using TestInyeccion.Interfaces;
using TestInyeccion.ViewModels;
using Xamarin.Forms;

namespace TestInyeccion.Views
{
    public partial class RegistrationPage : ContentPage
    {
        private readonly INavigator _navigator;
        private readonly IUserService _userService;
        private readonly IAuthService _authService;

        public RegistrationPage(
            INavigator navigator,
            IUserService userService,
            IAuthService authService)
        {
            _userService = userService;
            _authService = authService;
            _navigator = navigator;
            InitializeComponent();


            registerBtn.Clicked += async (sender, e) => {
                await OnRegistrationButton(sender, e);
            };
        }

        public async Task<int> OnRegistrationButton(object sender, EventArgs e)
        {
            Console.WriteLine("OnRegistrationButton");
            string st = String.Format("{0:r}", dateOfBirth);
            Console.WriteLine(password.Text);
            Console.WriteLine(name.Text);
            Console.WriteLine(lastname.Text);


            if (password.Text.Equals(passwordre.Text))
            {
                if (email.Text.Length > 0 && password.Text.Length > 0)
                {
                    FirebaseAuthLink authData =
                    await _authService.CreateUserWithEmailAndPasswordAsync(
                        email.Text,
                        password.Text);

                    string uid = authData.User.LocalId;

                    await _userService.SetUser(
                        uid,
                        name.Text,
                        email.Text,
                        lastname.Text,
                        dateOfBirth.Date.ToString());
                    await _navigator.PushAsync<MainMenuViewModel>();
                }
                else
                {
                }
            }
            else
            {
                //codigo para mensaje de error contracena no es igual
            }

            return 0;
        }
    }
}
