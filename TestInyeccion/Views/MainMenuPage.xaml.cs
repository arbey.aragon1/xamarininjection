﻿using System;
using System.Collections.Generic;
using TestInyeccion.Interfaces;
using TestInyeccion.ViewModels;
using Xamarin.Forms;

namespace TestInyeccion.Views
{
    public partial class MainMenuPage : ContentPage
    {
        private readonly INavigator _navigator;
        public MainMenuPage(INavigator navigator)
        {
            _navigator = navigator;
            InitializeComponent();
        }

        void OnProfileButton(object sender, EventArgs e)
        {
            Console.WriteLine("OnProfileButton");
            _navigator.PushAsync<ProfileViewModel>();
        }

        void OnMedicalRecordButton(object sender, EventArgs e)
        {
            Console.WriteLine("OnMedicalRecordButton");
            _navigator.PushAsync<MedicalRecordViewModel>();
        }

        void OnDocumentButton(object sender, EventArgs e)
        {
            Console.WriteLine("OnDocumentButton");
            _navigator.PushAsync<DocumentViewModel>();
        }
    }
}
