﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Firebase.Auth;
using TestInyeccion.Interfaces;
using TestInyeccion.Services;
using TestInyeccion.ViewModels;
using Xamarin.Forms;

namespace TestInyeccion.Views
{
    public partial class LoginPage : ContentPage
    {
        private readonly INavigator _navigator;
        private readonly IUserService _userService;
        private readonly IAuthService _authService;

        public LoginPage(
            INavigator navigator,
            IUserService userService,
            IAuthService authService)
        {
            _userService = userService;
            _authService = authService;
            _navigator = navigator;
            InitializeComponent();


            loginBtn.Clicked += async (sender, e) => {
                await OnLoginButton(sender, e);
            };
        }

        public async Task<int> OnLoginButton(object sender, EventArgs e)
        {
            Console.WriteLine("OnLoginButton");
            Console.WriteLine(email.Text);
            Console.WriteLine(password.Text);

            try
            {
                if (email.Text.Length > 0  && password.Text.Length > 0)
                {
                    FirebaseAuthLink authData = await _authService.SignInUserWithEmailAndPasswordAsync(email.Text, password.Text);
                    Console.WriteLine("===== token ========");
                    Console.WriteLine(authData.FirebaseToken);
                    await _navigator.PushAsync<MainMenuViewModel>();
                }
            }
            catch (Exception err)
            {
                //Error en la Autenticacion
            }

            return 0;
        }
    }
}
