﻿using System;
using System.Collections.Generic;
using TestInyeccion.Interfaces;
using Xamarin.Forms;

namespace TestInyeccion.Views
{
    public partial class ProfileEditorPage : ContentPage
    {
        private readonly INavigator _navigator;
        private readonly IUserService _userService;
        private readonly IAuthService _authService;

        public ProfileEditorPage(
            INavigator navigator,
            IUserService userService,
            IAuthService authService)
        {
            _userService = userService;
            _authService = authService;
            _navigator = navigator;
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            string uid = _authService.GetUserUID();

            var user = await _userService.FetchUserByUID(uid);

            userName.Text = user.Name;
            lastname.Text = user.LastName;
            email.Text = user.Email;
            //dateOfBirth.Date = Time(user.DateOfBirth);

        }

    }
}
