﻿using System;

using Xamarin.Forms;

namespace TestInyeccion.Pages
{
    public class Landing : ContentPage
    {
        public Landing()
        {
            Content = new StackLayout
            {
                Children = {
                    new Label { Text = "Hello ContentPage" }
                }
            };
        }
    }
}

