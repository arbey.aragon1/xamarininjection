﻿using System;
using TestInyeccion.Setup;
using TestInyeccion.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TestInyeccion
{
    public partial class App : Application
    {
        public App(AppSetup setup)
        {
            AppContainer.Container = setup.CreateContainer(this);
            //MainPage = new NavigationPage(new LandingPage());

            //var bootstrapper = new Bootstrapper(this);
            //bootstrapper.Run();
        }


        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

    }
}
