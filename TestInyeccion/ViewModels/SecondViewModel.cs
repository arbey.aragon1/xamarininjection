﻿using System;
using System.Windows.Input;
using TestInyeccion.Interfaces;
using Xamarin.Forms;

namespace TestInyeccion.ViewModels
{
    public class SecondViewModel : BaseViewModel
    {
        public SecondViewModel(IHelloFormsService formsService)
        {
            HelloText = formsService.GetHelloFormsText();
        }

        public string HelloText { get; set; }
    }
}
