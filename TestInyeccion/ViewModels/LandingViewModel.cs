﻿using System;
using TestInyeccion.Interfaces;

namespace TestInyeccion.ViewModels
{
    public class LandingViewModel : BaseViewModel
    {
        public LandingViewModel(IHelloFormsService formsService)
        {
            HelloText = formsService.GetHelloFormsText();
        }

        public string HelloText { get; set; }
    }
}
