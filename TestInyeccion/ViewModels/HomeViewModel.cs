﻿using System;
using TestInyeccion.Interfaces;

namespace TestInyeccion.ViewModels
{
    public class HomeViewModel
    {
        public HomeViewModel(IHelloFormsService formsService)
        {
            HelloText = formsService.GetHelloFormsText();
        }

        public string HelloText { get; set; }
    }
}
