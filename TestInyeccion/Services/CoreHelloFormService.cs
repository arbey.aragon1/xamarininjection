﻿using System;
using TestInyeccion.Interfaces;

namespace TestInyeccion.Services
{
    public class CoreHelloFormService : IHelloFormsService
    {
        public string GetHelloFormsText()
        {
            return "hello";
        }
    }
}
