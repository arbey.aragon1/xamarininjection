﻿using System;
using System.Threading.Tasks;
using Firebase.Auth;
using Firebase.Database;
using TestInyeccion.Interfaces;

namespace TestInyeccion.Services
{
    public class AuthService : IAuthService
    {
        private IFirebaseService _firebaseService;

        public AuthService(IFirebaseService firebaseService)
        {
            _firebaseService = firebaseService;
        }

        public async Task<FirebaseAuthLink> CreateUserWithEmailAndPasswordAsync(string email, string password)
        {
            return await this._firebaseService.CreateUserWithEmailAndPasswordAsync(email, password);
        }

        public async Task<FirebaseAuthLink> SignInUserWithEmailAndPasswordAsync(string email, string password)
        {
            return await this._firebaseService.SignInUserWithEmailAndPasswordAsync(email, password);
        }

        public FirebaseAuthLink GetAuthData()
        {
            return this._firebaseService.GetAuthData();
        }

        public string GetUserUID()
        {
            return this._firebaseService.GetUserUID();
        }

        public bool HasAuth()
        {
            return this._firebaseService.HasAuth;
        }
    }
}
