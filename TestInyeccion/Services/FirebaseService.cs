﻿using System;
using Firebase.Database;
using Firebase.Database.Query;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using Firebase.Auth;
using TestInyeccion.Interfaces;

namespace TestInyeccion.Services
{
    
    public class FirebaseService : IFirebaseService 
    {
        private FirebaseClient firebase;
        private FirebaseAuthLink authUser;
        //private const string ApiKey = "AIzaSyD9yuZ8NtpKw4K3vtIaVI04KVsvAA7PERg";
        //private const string URL = "https://pehrapp-2c323.firebaseio.com/";

        private const string ApiKey = "AIzaSyCGipG7NGxUUSBM8vJPYVY3AI0vh8M6gw4";
        private const string URL = "https://personalehr-prod.firebaseio.com/";


        private const string FacebookAccessToken = "<FACEBOOK USER ACCESS TOKEN>";
        private const string FacebookTestUserFirstName = "Mark";

        private const string GoogleAccessToken = "<GOOGLE USER ACCESS TOKEN>";
        private const string GoogleTestUserFirstName = "Mark";

        public bool HasAuth { set; get; }

        public FirebaseService()
        {

        }

        private void CreateClient(FirebaseAuthLink auth)
        {
            this.firebase = new FirebaseClient(
              URL,
              new FirebaseOptions
              {
                  AuthTokenAsyncFactory = () => Task.FromResult(auth.FirebaseToken)
              });
        }

        public async Task<FirebaseAuthLink> CreateUserWithEmailAndPasswordAsync(string email, string password)
        {
            var authProvider = new FirebaseAuthProvider(new FirebaseConfig(ApiKey));

            this.authUser = await authProvider.CreateUserWithEmailAndPasswordAsync(email, password);

            CreateClient(this.authUser);

            return this.authUser;
        }


        public async Task<FirebaseAuthLink> SignInUserWithEmailAndPasswordAsync(string email, string password)
        {
            var authProvider = new FirebaseAuthProvider(new FirebaseConfig(ApiKey));

            this.authUser = await authProvider.SignInWithEmailAndPasswordAsync(email, password);

            CreateClient(this.authUser);

            return this.authUser;
        }

        public FirebaseAuthLink GetAuthData()
        {
            return this.authUser;
        }

        public string GetUserUID()
        {
            return this.authUser.User.LocalId;
        }

        public FirebaseClient GetDBReference()
        {
            return this.firebase;
        }
    }
}
