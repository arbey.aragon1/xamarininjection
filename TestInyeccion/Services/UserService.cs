﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Firebase.Database;
using Firebase.Database.Query;
using TestInyeccion.Interfaces;
using TestInyeccion.Models;

namespace TestInyeccion.Services
{
    public class UserService : IUserService
    {
        private IFirebaseService _firebaseService;
        private readonly string usersKey = "Users"; 

        public UserService(IFirebaseService firebaseService)
        {
            _firebaseService = firebaseService;
        }


        public async Task<List<User>> FetchUserList()
        {
            return (await this._firebaseService
              .GetDBReference()
              .Child(usersKey)
              .OnceAsync<User>())
              .Select(item => new User
              {
                  Name = item.Object.Name,
                  LastName = item.Object.LastName,
                  DateOfBirth = item.Object.DateOfBirth,

              }).ToList();
        }

        public async Task<User> FetchUserByUID(string uid)
        {
            return (await this._firebaseService
              .GetDBReference()
              .Child(usersKey + "/" + uid)
              .OnceSingleAsync<User>());
        }

        public async Task AddUser(
            string name,
            string lastName,
            string dateOfBirth)
        {
            await this._firebaseService
              .GetDBReference()
              .Child(usersKey)
              .PostAsync(new User() {
                  Name = name,
                  LastName = lastName,
                  DateOfBirth = dateOfBirth
              });
        }



        public async Task SetUser(
            string uid,
            string name,
            string email,
            string lastName,
            string dateOfBirth)
        {
            await this._firebaseService
              .GetDBReference()
              .Child(usersKey+"/"+uid)
              .PutAsync(new User()
                {
                    Name = name,
                    LastName = lastName,
                    DateOfBirth = dateOfBirth,
                    Email = email
                });
        }
    }
}
