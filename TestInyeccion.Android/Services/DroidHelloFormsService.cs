﻿using System;
using TestInyeccion.Interfaces;

namespace TestInyeccion.Droid.Services
{
    public class DroidHelloFormsService : IHelloFormsService
    {
        public string GetHelloFormsText()
        {
            return "Hello from android";
        }
    }
}
