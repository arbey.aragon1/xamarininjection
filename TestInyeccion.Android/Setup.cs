﻿using System;
using Autofac;
using TestInyeccion.Droid.Services;
using TestInyeccion.Interfaces;
using TestInyeccion.Setup;

namespace TestInyeccion.Droid
{
    public class Setup : AppSetup
    {
        protected override void RegisterDependencies(ContainerBuilder container)
        {
            base.RegisterDependencies(container);
            container.RegisterType<DroidHelloFormsService>().As<IHelloFormsService>();
        }
    }
}
